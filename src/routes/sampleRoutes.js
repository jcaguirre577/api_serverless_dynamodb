/**
 * @swagger
 * /api/sample:
 *   get:
 *     tags:
 *       - Sample
 *     description: Retorna un mensaje de muestra
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Mensaje de muestra
 */
app.get('/', (req, res) => {
    res.json({ message: 'Este es un mensaje de muestra' });
  });

// Método GET para obtener todos los elementos
router.get('/api/sample1', (req, res) => {
  res.json('Perfect. method GET');
});

// Método POST para agregar un nuevo elemento
routes.post('/api/sample2', (req, res) => {
  const newItem = req.body; // Suponemos que envías datos en el cuerpo de la solicitud
  data.push(newItem);
  res.status(201).json('Perfect. method POST');
});

// Método PUT para actualizar un elemento existente por su ID
routes.put('/:id', (req, res) => {
  const id = req.params.id;
  const updatedItem = req.body; // Suponemos que envías los datos de actualización en el cuerpo de la solicitud

  // Busca el elemento por su ID y actualízalo
  const index = data.findIndex(item => item.id === id);

  if (index !== -1) {
    data[index] = updatedItem;
    res.json(updatedItem);
  } else {
    res.status(404).json({ message: 'Elemento no encontrado' });
  }
});

// Método DELETE para eliminar un elemento por su ID
routes.delete('/:id', (req, res) => {
  const id = req.params.id;

  // Busca el elemento por su ID y elimínalo
  const index = data.findIndex(item => item.id === id);

  if (index !== -1) {
    const deletedItem = data.splice(index, 1);
    res.json(deletedItem[0]);
  } else {
    res.status(404).json({ message: 'Elemento no encontrado' });
  }
});

module.exports = sampleRoutes;
  
  