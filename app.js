const express = require('express');
const app = express();
const port = 3000;
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const morgan = require('morgan');
const router = express.Router();


// Middleware Morgan para el registro de solicitudes
app.use(morgan('combined')); 


// Swagger setup
const swaggerDefinition = {
    info: {
      title: 'API Serverless, DynamoDB',
      version: '1.0.0',
      description: 
      'Documentación de la API de ejemplo con sus metodos ( GET, POST, PUT, DELETE ), usando los servicios de AWS ',
    },
    host: `localhost:${port}`,
    basePath: '/',
  };
  
  const options = {
    swaggerDefinition,
    apis: ['/routes/sampleRoutes'], // Ruta a tus archivos de definición de rutas
  };
  
  const swaggerSpec = swaggerJSDoc(options);
  
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// Rutas 
app.get('src/routes/sampleRoutes', (req, res) => {
  res.send('Bienvenido a Serverless');
});

app.get('/routes/sampleRoutes', (req, res) => {
    res.send('A=B');
});

app.post('/routes/sampleRoutes', (req, res) => {
    res.send('A=B');
});

app.delete('/routes/sampleRoutes', (req, res) => {
    res.send('A=B');
});

// Iniciar el servidor
app.listen(port, () => {
  console.log(`Servidor escuchando en http://localhost:${port}`);
});
